package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.NewPostService;


@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("newpost.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> posts = new ArrayList<String>();

        if (isValid(request, posts) == true) {

            User user = (User) session.getAttribute("loginUser");

            Post post = new Post();
            post.setUserId(user.getId());
            post.setTitle(request.getParameter("title"));
            post.setCategory(request.getParameter("category"));
            post.setText(request.getParameter("text"));
            System.out.println(post);


            new NewPostService().register(post);
            session.setAttribute("post", post); // ついか
            System.out.println(session);
            response.sendRedirect("./top");
        } else {
            session.setAttribute("errorMessages", posts);
            response.sendRedirect("newpost.jsp");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("text");
        String title = request.getParameter("title");
        String category = request.getParameter("category");


        if (StringUtils.isEmpty(text) == true) {
            messages.add("メッセージを入力してください");
        }
        if (StringUtils.isEmpty(title) == true) {
            messages.add("タイトルを入力してください");
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (1000 < text.length()) {
            messages.add("1000文字以下で入力してください");

        }
        if (30 < title.length()) {
            messages.add("タイトルは30文字以下で入力してください");

        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");

        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}