package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));
			user.setBranchCode(request.getParameter("branch_code"));
			user.setDepartment(request.getParameter("department"));
			user.setAuthority(request.getParameter("authority"));

			new UserService().register(user);

			response.sendRedirect("login.jsp");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String branch = request.getParameter("branch_code");
		String department = request.getParameter("department");
		String authority = request.getParameter("authority");


		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(branch) == true) {
			messages.add("支店名を入力してください");
		}
		if (StringUtils.isEmpty(department) == true) {
			messages.add("部署を入力してください");
		}
		if (login_id.length()<6 || login_id.length()>20){
			messages.add("アカウント名は6～20文字以下の英数字で設定してください");
		}
////		if (!(login_id.matches("\\W*"))){
////			messages.add("アカウント名は半角英数字で設定してください");
//		}
		if (name.length() > 10){
			messages.add("名前は10文字以下で設定してください");
		}

		if(StringUtils.isEmpty(authority)==true){
			messages.add("ユーザーの状態を入力してください");
		}

		//		if (account.length() < 6 || 20 < account.length() ){
		//			messages.add("ログインIDは6文字以上20文字以下で設定してください");
		//
		//		}else if(!account.matches("[0-9A-Za-z]+$")){
		//			messages.add("ログインIDは半角英数字で設定してください");
		//			}else {
		//
		//			}


		if (!(password.equals(passwordConfirm))){
			messages.add("確認用パスワードと一致しません");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}