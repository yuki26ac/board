package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserPost;
import service.NewPostService;



@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



        List<UserPost> posts = new NewPostService().getMessage();

        request.setAttribute("posts", posts);



        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}