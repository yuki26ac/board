package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String login_id;
	private String name;
	private String password;
	private String branch_code;
	private String department;
	private String authority;
	private Date created_date;
	private Date updated_date;


	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return this.login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBranchCode() {
		return this.branch_code;
	}
	public void setBranchCode(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getDepartment() {
		return this.department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Date getCreatedDate() {
		return this.created_date;
	}
	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdatedDate() {
		return this.updated_date;
	}
	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

	public String getAuthority() {
		return this.authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
}