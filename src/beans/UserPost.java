package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private int userId;
    private String text;
    private String title;
    private String category;
    private Timestamp created_date;
    private Date updated_date;
    private String login_id;
    private String searchCategory;
	private String searchDate;


public int getId(){
	return id;
}
public void setId(int id){
	this.id= id ;
}
public String getName(){
	return name;
}
public void setName(String name){
	this.name = name;
}
public int getUserId(){
	return userId;
}
public void setUserId(int userId){
	this.userId =userId;
}
public String getText(){
	return text;
}
public void setText(String text){
	this.text = text;
}
public String getTitle(){
	return title;
}
public void setTitle(String title){
	this.title =title;
}
public String getCategory(){
	return category;
}
public void setCategory(String category){
	this.category = category;
}

public Timestamp getCreatedDate(){
	return created_date;
}
public void setCreatedDate(Timestamp created_date){
	this.created_date = created_date;
}
public Date getUpdatedDate(){
	return updated_date;
}
public void setUpdatedDate(Date updated_date){
	this.updated_date = updated_date;
}
public String getLoginId() {
	return login_id;
}
public void setLoginId(String login_id) {
	this.login_id = login_id;
}
public String getSearchCategory(){
	return searchCategory;
}

public void setSearchCategory(String searchCategory){
	this.searchCategory = searchCategory;
}



public String getSearchDate(){
	return searchDate;
}
public void setSearchDate(String searchDate){
	this.searchDate = searchDate;
}
}


//thisの意味