package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				String post_id = rs.getString("post_id");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setLoginId(login_id);
				comment.setName(name);
				comment.setId(id);
				comment.setUserId(userId);
				comment.setText(text);
				comment.setPostId(post_id);
				comment.setCreatedDate(created_date);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}

