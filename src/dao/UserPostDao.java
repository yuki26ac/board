package dao;


import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPost(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.text as text, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.category as category,");
			sql.append("posts.title as title,");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				String title = rs.getString("title");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserPost post = new UserPost();
				post.setLoginId(login_id);
				post.setName(name);
				post.setId(id);
				post.setUserId(userId);
				post.setText(text);
				post.setCategory(category);
				post.setTitle(title);
				post.setCreatedDate(created_date);


				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
