<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
	<div class="profile">
		<div class="name">
			<h2>
				<c:out value="${loginUser.name}" />

				さんようこそ
			</h2>
		</div>
	</div>


	<a href="newpost.jsp">新規投稿</a>
	<br />
	<br />
	<br />



	<div class="posts">
		<c:forEach items="${posts}" var="post">
			<div class="post">
				<div class="login_id-name">
					<font size=4><span class="login_id"><c:out
								value="${post.loginId}" /></span> <span class="name"> <c:out
								value="${post.name}" /></span></font><br>

				</div>
				<div class="title">
					title：
					<c:out value="${post.title}" />
				</div>
				<div class="category">
					category：
					<c:out value="${post.category}" />
				</div>
				<div class="text">
					<font size=4> <c:out value="${post.text}" /><br>
					<br>
					</font>
				</div>
			</div>
		</c:forEach>
	</div>





	<c:if test="${ not empty loginUser }">

		<a href="logout">ログアウト</a>
	</c:if>

	<br />
	<a href="login.jsp">戻る</a>
</body>
</html>
