<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>

<body>
<body bgcolor="	#FFFF00">
	<font size=6>ユーザー登録画面</font>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="name">名前（10字以内)</label> <br /> <input
				name="name" id="name" /><br /> <br /> <label for="login_id">アカウント名(半角英数字6-20字以内)</label>
			<br /> <input name="login_id" id="login_id" /> <br /> <br /> <label
				for="password">パスワード(半角文字6-20字以内)</label> <br /> <input
				name="password" type="password" id="password" /> <br /> <br /> <label
				for="passwordConfirm">パスワード(確認用)</label> <br /> <input
				name="passwordConfirm" type="password" id="passwordConfirm" /><br />
			<br /> <label for="branch_code">支店</label><br /> <input
				name="branch_code" /><br /> <br /> <label for="department">部署・役職</label><br />
			<input name="department" /><br /> <label for="authority">ユーザーの状態</label> <br />
			<input name="authority" type="authority" id="authority" />
			<br /> <input type="submit" value="登録" /> <br /> <a href="login.jsp">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</body>

</html>