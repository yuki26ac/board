<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<title>投稿画面</title>
</head>
<body>
	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="login_id">
				<c:out value="${loginUser.loginId}" />
			</div>

		</div>
	</c:if>

	新規投稿
	<br />

<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

	<form action="newpost" method="post">
		<br /> <label for="title">Title</label> <input name="title"
			value="${tittle}" id=title /> <br /> <label for="category">Category</label>
		<textarea name="category" cols="35" rows="1" id="category"></textarea>
		<br /> <br />
		<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
		<br /> <input type="submit" value="投稿">（1000文字まで）
	</form>
	<form action="top" method="get">
	<input type="submit" value="戻る">
	</form>

</body>
</html>