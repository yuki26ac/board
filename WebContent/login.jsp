<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<font size=6>掲示板</font>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post">
		<p>
			<br /> <label for="login_id">ログインID</label> <input name="login_id"
				value="${loginUser.loginId}" id="login_id" /> <br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /> <br /> <input type="hidden" name="authority"
				value="${loginUser.authority}"> <input type="submit"
				value="ログイン" /> <br /> <a href="signup">新規登録</a>
				</p>
		</form>

	</div>
	<div class="copylight">Copyright(c)YukiDeguchi</div>
</body>
</html>